
const Koa = require('koa');
const api = require('./api');

const app = new Koa();
app
  .use(api.routes())
  .use(api.allowedMethods());


app.listen(3000);
console.log("Listening on port 3000");