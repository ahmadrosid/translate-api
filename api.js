var Router = require('koa-router');
const api = new Router();
const translate = require('google-translate-api');

api.get('/translate', async (ctx, next) => {
	let q = ctx.query;
	if (!q.from) {
	    ctx.body = {
	    	succes: false,
	    	message: 'Please input source language.'
	    };
	}else if (!q.to) {
	    ctx.body = {
	    	succes: false,
	    	message: 'Please input target language.'
	    };
	}else if (!q.q) {
	    ctx.body = {
	    	succes: false,
	    	message: 'Please input query language.'
	    };
	}else{
		await translate(q.q, {from: q.from, to: q.to}).then(res => {
		    console.log(res.text);
		    console.log(res.from.language.iso);
		    ctx.body = res;
		}).catch(err => {
		    console.error(err);
		});
	}
});

module.exports = api;